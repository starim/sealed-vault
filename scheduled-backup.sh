#!/usr/bin/env bash

# Creates a snapshot of the Firebase database at the given url and saves to
# "snapshots/<name>-<date>.log" relative to the Sealed Vault binary. This
# script can be invoked manually from the command line, or scheduled to run
# e.g. every hour by adding the following crontab line:
#
# 0 * * * * /<absolute-path>/scheduled-backup.sh
#
# Fill in the variables below to customize for your database.

name="a short name for your database"
url="your database's url"

# END CONFIGURATION SECTION


absolute_script_dir=$(cd -P "$(dirname "${BASH_SOURCE[0]}")" && pwd)
cd "$absolute_script_dir"
mkdir -p snapshots
$(RUST_LOG=info ./sealed-vault snapshot "$url" 1>snapshots/"$name-$(date --iso-8601=seconds).json" 2>>sealed-vault.log)

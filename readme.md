Sealed Vault
==

A tool to save snapshots of a Firebase database, with the ability to restore the database from a snapshot.

Installing
--

1. [Install Rust](https://www.rust-lang.org/downloads.html)
2. Make sure the curl development library is installed: `sudo apt-get install libcurl-dev` (Debian/Ubuntu) or `sudo yum install curl-devel` (Red Hat/CentOS)
2. Clone the repo: `git clone https://github.com/starim/sealed-vault.git <folder you want it in>`
3. `cd` into the folder where Sealed Vault was cloned to
4. Build Sealed Vault: `cargo build --release`
5. Copy the executable at `target/release/sealed-vault` onto your server
6. ssh into your server and execute Sealed Vault from the CLI, as described below:

Authentication
--

Not yet implemented.

Taking a Manual Snapshot
--

Executing `sealed-vault snapshot <url>` prints the JSON-encoded contents of the
Firebase database at `url`. To save the contents to a file, use shell
redirection: `sealed-vault snapshot <url> > archive.json`

Sealed vault sends log messages to stderr. To set logging level, prepend the
executable with `RUST_LOG=<log level>`. To capture log messages in a file,
append `2><log file>`.

For example:
```
RUST_LOG=info ./sealed-vault snapshot <url> 2>sealed-vault.log
```

Scheduling Automatic Snapshots
--

The `scheduled-backup.sh` script can be invoked by cron on a set schedule to
perform automatic snapshots if placed in the same directory as the
`sealed-vault` binary. Customize the variables at the top of
`scheduled-backup.sh` to be appropriate for your database, then add a crontab
line like the following (this example takes an hourly backup):

```
0 * * * * /<absolute-path>/scheduled-backup.sh
```

Snapshots will be placed in a `snapshots` folder in the same directory as the
`scheduled-backup.sh` script and a log named `sealed-vault.log` will also be
kept in the same folder as `scheduled-backup.sh`.

Restoring a Snapshot
--

Not yet implemented.

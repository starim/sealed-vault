extern crate chrono;
extern crate docopt;
extern crate env_logger;
extern crate firebase;
#[macro_use] extern crate log;
extern crate logger;
extern crate rustc_serialize;

use chrono::UTC;
use docopt::Docopt;
use firebase::Firebase;

const USAGE: &'static str = "
sealed-vault - A tool to save snapshots of a Firebase database, with the ability to restore the
database from a snapshot.

Usage:
  sealed-vault snapshot [--auth=<token>] <url>
  sealed-vault restore [--auth=<token>] <url> <snapshot_file>
  sealed-vault (-h | --help)

Options:
  -h --help        Show help text.
  --auth=<token>   Auth token to log into Firebase
";

#[derive(Debug, RustcDecodable)]
struct Args {
	cmd_snapshot: bool,
	cmd_restore: bool,
	flag_auth: Option<String>,
	arg_url: String,
	arg_snapshot_file: Option<String>,
}

fn main() {
    env_logger::init().expect("Failed to initialize logging backend.");
    let args: Args = Docopt::new(USAGE)
                            .and_then(|d| d.decode())
                            .unwrap_or_else(|e| e.exit());

	let firebase = match args.flag_auth {
		Some(ref token) => match Firebase::authed(&*args.arg_url, token) {
			Ok(firebase) => firebase,
			Err(error) => {
				panic!("[{}] Invalid Firebase URL: {:?}", UTC::now(), error)
			}
		},
		None => match Firebase::new(&*args.arg_url) {
			Ok(firebase) => firebase,
			Err(error) => {
				panic!("[{}] Invalid Firebase URL: {:?}", UTC::now(), error)
			}
		}
	};

	if args.cmd_snapshot {
		info!("[{}] Starting snapshot of {}", UTC::now(), args.arg_url);
		// the unwrap is safe since "/" is guaranteed to be a valid path
		let scope = firebase.at("/").unwrap();
		let content = match scope.get() {
			Ok(response) => response,
			Err(error) => {
				panic!("[{}] Error communicating with Firebase: {:?}", UTC::now(), error);
			}
		};
		info!("[{}] Successfully retrieved snapshot of {}", UTC::now(), args.arg_url);
		println!("{}", content.body);
	}

	if args.cmd_restore {
		unimplemented!();
	}

}
